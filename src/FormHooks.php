<?php

namespace Drupal\ticketforum;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

class FormHooks {

  /**
   * Implements hook_entity_prepare_form().
   *
   * When creating a new issue, prepopulate ticketforum_id.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public static function hookEntityPrepareForm(EntityInterface $entity, $operation, FormStateInterface $form_state) {
    if ($entity instanceof NodeInterface) {
      if ($entity->bundle() === 'ticketforum') {
        if ($operation === 'default') {
          if ($ticketforumId = \Drupal::request()->query->get('ticketforum_id')) {
            if ($entity->hasField('field_taxonomy_ticketforums')) {
              $entity->get('field_taxonomy_ticketforums')->setValue($ticketforumId);
            }
          }
        }
      }
    }
  }

  public static function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if ($form_id === 'comment_form') {
      static::hookFormCommentFormAlter($form, $form_state, $form_id);
    }
  }

  /**
   * Implements hook_form_BASE_FORM_ID_alter() for \Drupal\comment\CommentForm.
   *
   * We use field_ticketforum_topic_status to open/close comments. The field can
   * be extended so that every status starting with 'open' makes comments open.
   */
  public static function hookFormCommentFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (\Drupal::currentUser()->hasPermission('ticketforum edit closed forum topic')) {
      return;
    }
    /** @var \Drupal\comment\CommentForm $commentForm */
    $commentForm = $form_state->getFormObject();
    /** @var \Drupal\comment\Entity\Comment $comment */
    $comment = $commentForm->getEntity();
    $commentedEntity = $comment->getCommentedEntity();
    if ($commentedEntity->hasField('field_ticketforum_topic_status')) {
      $topicStatus = $commentedEntity->get('field_ticketforum_topic_status')->getString();
      if (substr($topicStatus, 0, 4) !== 'open') {
        $form['#access'] = FALSE;
      }
    }
  }

  /**
   * Implements hook_field_widget_form_alter().
   *
   * Restrict term description to markup format, allowed_formats can't do it yet.
   *
   * @see allowed_formats_field_widget_form_alter
   */
  public static function hookFieldWidgetFormAlter(&$element, FormStateInterface $form_state, $context) {
    /** @var FieldItemListInterface $items */
    $items = $context['items'];
    $fieldDefinition = $items->getFieldDefinition();
    if (
      $fieldDefinition->getTargetEntityTypeId() === 'taxonomy_term'
      && $fieldDefinition->getName() === 'description'
    ) {
      $element['#allowed_formats'] = ['markdown' => 'markdown'];
      $element['#allowed_format_hide_settings'] = [
        'hide_help' => TRUE,
        'hide_guidelines' => TRUE
      ];
      $element['#after_build'][] = '_allowed_formats_remove_textarea_help';
    }
  }

  /**
   * Implements hook_menu_local_tasks_alter().
   *
   * We want all node edits to happen via comments. We can not change access to
   * edit, as this would prevent node edit via comment, too.
   * So for now we simply remove the edit tab.
   *
   * @todo Consider redirecting node edit to comment add.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public static function hookMenuLocalTasksAlter(&$data, $route_name, \Drupal\Core\Cache\RefinableCacheableDependencyInterface &$cacheability) {
    if ($route_name === 'entity.node.canonical') {
      $viewUrl =& $data['tabs'][0]['entity.node.canonical']['#link']['url'];
      $editUrl =& $data['tabs'][0]['entity.node.edit_form']['#link']['url'];
      if ($viewUrl instanceof Url && $editUrl instanceof Url) {
        $routeParameters = $viewUrl->getRouteParameters();
        if ($nodeId = $routeParameters['node'] ?? NULL) {
          /** @var \Drupal\taxonomy\Entity\Term $term */
          $node = Node::load($nodeId);
          if ($node->bundle() === 'forum') {
            unset($data['tabs'][0]['entity.node.edit_form']);
          }
        }
      }
    }
  }

}
